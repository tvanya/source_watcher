#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os
import sys
import time
import subprocess
import logging
import warnings

from psd_tools import PSDImage
from watchdog.observers import Observer
from ConfigParser import SafeConfigParser

# sets colors for logging.info/warning/error
import Colorer

import psdhandler

def formatMessage(event, path):
    return "{} {}".format(event.src_path.replace(path, ".."), event.event_type)

# http://code.activestate.com/recipes/576602-safe-print/
def safe_print(u, errors="replace"):
    """Safely print the given string.
    
    If you want to see the code points for unprintable characters then you
    can use `errors="xmlcharrefreplace"`.
    """
    # u = u.decode(sys.getfilesystemencoding(), errors)
    print u
    return u.encode(sys.stdout.encoding or "utf-8", errors)

class SourceWatcher(object):
    """docstring for SourceWather"""

    def __init__(self, path):
        if type(path) == 'str':
            path = path.decode('utf-8')
        self._path = path
        # self._path = path
        
        self._mat_dirname = path.split(os.sep)[-1]
        if self._mat_dirname == '':
            self._mat_dirname = path.split(os.sep)[-2]

        self._rel_path = ''
        self._rel_file_path = ''

        parent_dir = os.path.abspath(os.path.join(__file__, os.pardir))
        config_path = os.path.join(parent_dir, 'config.ini')

        self._config = SafeConfigParser()
        self._config.read(config_path)

        self._map_name = self._config.get('project_settings', 'map_name')
        self._csgo_path = self._config.get('project_settings', 'csgo_path')
        self._bin_path = os.path.join(self._csgo_path, 'bin')
        self._vtex = os.path.join(self._bin_path, 'vtex.exe')

        self._event_handler = psdhandler.PsdHandler(on_modified = self.process, on_created = self.process)
        if os.path.exists(self._csgo_path):
            self._observer = Observer()
            self._observer.schedule(self._event_handler, self._path, recursive=True)
            self._observer.start()

            logging.info('Started watching \"{}\"'.format(self._path))
            logging.info('Will output into \"{}\"'.format(os.path.join('csgo', 'materials', self._map_name)))
            logging.error('Type \'exit\' to stop Watcher')
        else:
            sys.exit(logging.info('CSGO not found!\nPlease make sure to set up your config.ini'))

    def stop(self):
        if self._observer:
            self._observer.stop()
            self._observer.join()

    def checkPath(self, event):
        # splitting the texture's path into a relative path
        if self._mat_dirname in event.src_path:
            self._rel_file_path = event.src_path.split(self._mat_dirname)[1]  # makes a nice relative path eg. 'dev\cube.psd'
            self._rel_path = self._rel_file_path.split(os.sep)[:-1]  # splitting string into a list without the last item
            self._rel_path = os.path.join(*self._rel_path)  # did this to get rid of the first backslash
            return True
        return False

    def cleanup(self, outDir):
        outDirList = os.listdir(outDir)
        for j in outDirList:
            if j.endswith('pwl.vtf'):
                logging.info('Performing cleanup: {}'.format(j))
                os.remove(os.path.join(outDir, j))

    def getPSDInfo(self, event):
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            if os.path.exists(event.src_path):
                psd = PSDImage.load(event.src_path)
                firstGroupName = psd.layers[0].name
                return firstGroupName
            return None

    def on_modified(self, event):
        self.process(event)
        if self.getPSDInfo(event) == 'mdl':
          self.cleanup(os.path.join(self._csgo_path, 'csgo', 'materials', 'models', self._map_name))
        else:
          self.cleanup(os.path.join(self._csgo_path, 'csgo', 'materials', self._map_name))

    def on_created(self, event):
        self.process(event)

    def process(self, event):
        if not self.checkPath(event):
            # todo, weird shit 
            return

        # get relative path to parent directory
        path = event.src_path.replace(self._path, '')
        # if it doesnt end with os.sep, add it
        if path[1] != os.sep:
            path = os.sep + path
        path = '..' + path
        logging.info('{} {}'.format(path, event.event_type))
        # result: ../folder/file.psd depending on os.sep

        if event.event_type != 'modified':
            return
        firstGroupName = self.getPSDInfo(event)

        src_path = event.src_path
        csgo_path = self._csgo_path
        vtex = self._vtex
        rel_path = self._rel_path
        map_name = self._map_name

        if firstGroupName == 'novmt':
            args = [vtex, '-quiet', '-nopause', '-game', os.path.join(csgo_path, 'csgo'), '-outdir', os.path.join(csgo_path, 'csgo', 'materials', map_name, rel_path), '-nop4', src_path]
        elif firstGroupName == 'mdl':
            args = [vtex, '-quiet', '-nopause', '-game', os.path.join(csgo_path, 'csgo'), '-outdir', os.path.join(csgo_path, 'csgo', 'materials', 'models', map_name), '-shader', 'VertexLitGeneric', '-nop4', src_path]
        elif 'mdl' and 'novmt' in firstGroupName:
            args = [vtex, '-quiet', '-nopause', '-game', os.path.join(csgo_path, 'csgo'), '-outdir', os.path.join(csgo_path, 'csgo', 'materials', 'models', map_name), '-nop4', src_path]
        else:
            args = [vtex, '-quiet', '-nopause', '-game', os.path.join(csgo_path, 'csgo'), '-outdir', os.path.join(csgo_path, 'csgo', 'materials', map_name, rel_path), '-shader', 'LightmappedGeneric', '-nop4', src_path]

        # encode all fields in list args into utf-8 byte sequence
        byteargs = [x.encode('utf-8') for x in args]

        FNULL = open(os.devnull, 'w')
        subprocess.Popen(byteargs, stdout=FNULL, stderr=subprocess.STDOUT)

if __name__ == "__main__":
    logging.basicConfig(level = logging.INFO,
                        format = '%(asctime)s - %(message)s',
                        datefmt = '%Y-%m-%d %H:%M:%S')

    # check to see if we have csgo in place
    path = sys.argv[1] if len(sys.argv) > 1 else sys.exit(logging.error('No path given to watch.'))
    # from kitchen.text.converters import getwriter
    import codecs
    UFT8Writer = codecs.getwriter(sys.stdout.encoding)
    sys.stdout = UFT8Writer(sys.stdout)

    try:
        watcher = SourceWatcher(path)
        while(True):
            if raw_input() == 'exit':
                watcher.stop()
                sys.exit(0)
                break;
    except KeyboardInterrupt:
        watcher.stop()
        sys.exit(0)
from watchdog.events import PatternMatchingEventHandler

class PsdHandler(PatternMatchingEventHandler):
    """docstring for PsdHandler"""
    def __init__(self, on_modified=None, on_created=None):
        super(PsdHandler, self).__init__()
        self._on_modified = on_modified
        self._on_created = on_created

    patterns = ["*.psd"]

    def on_modified(self, event):
        if self._on_modified:
            self._on_modified(event)

    def on_created(self, event):
        if self._on_created:
            self._on_created(event)
